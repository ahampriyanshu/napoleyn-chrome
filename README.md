# Napoleyn

A chrome extension from Napoleyn Suite to keep track of upcoming and ongoing contests

## Installation

* Navigate to chrome://extensions
* Expand the Developer dropdown menu 
* Click “Load Unpacked Extension”

## Screenshot

![loading](images/napoleyn-ext.png)